import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Navbar from '../../components/Navbar/navbar';
import Main from '../../components/Main/main';


function Routing() {
    return(
        <Router>
            <Switch>
                <Route exact path='/'>
                    <Navbar></Navbar>
                <Main />
                </Route>
                
            </Switch>
        </Router>
    )
    
}

export default Routing;