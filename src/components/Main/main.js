import React, { useRef } from 'react';
import Banner from '../../assets/banner.jpg';
import Navbar from '../Navbar/navbar';
import './main.scss';
import Pencil from '../../assets/pencil.gif'
import Image1 from '../../assets/img1.jpg';
import Image2 from '../../assets/img2.jpg';
import Facebook from '../../assets/facebook.png';
import LinkedIn from '../../assets/linkedin.png';



export default function Main() {


    return (
        <div>
            <main>
                <div className="container">
                    <article className="article-card">
                        <h2>HOW WE DO IT</h2>
                        <h3>We use data-driven creativity to solve business problems</h3>
                        <img className="article-img" src={Banner} type="image/png"></img>

                        <div className="article-content">
                            <p>
                                By harnessing powerful insights and targetting, we're able to create behaviour-changing ideas and experiences that value to brands. Supported by
            our proprietary <u>Creative Intelligence</u> <u>process</u> and <u>global partners</u>, we are able
            to convert data into visuals reaching the heart of everything we do to orchestrate experiences
            that deliver creativity with precision and purpose.
            </p>
                        </div>
                    </article>

                    <article>

                        <img className="article-pencil" src={Pencil} type="image/gif"></img>


                        <div className="article-minicard">
                            <h2>BUSINESS PROBLEMS WE'VE SOLVED</h2>
                            <div className="article-minicard-left">
                                <img className="article-minicard-img1" src={Image1} type="image/png"></img>
                                <div className="minicard-text">
                                    <h5>Can a love story double as a hearing test</h5>
                                    <span><a href="#">Find out more</a></span>
                                </div>

                            </div>
                            <div className="article-minicard-right">
                                <img className="article-minicard-img2" src={Image2} type="image/png"></img>
                                <div className="minicard-text">
                                    <h5>How close to AFL player can you get</h5>
                                    <span><a href="#">Find out more</a></span>
                                </div>
                            </div>
                            <a className="vwork" href="#">View work</a>
                        </div>
                        <div className="arrow-up">

                            <i class="fal fa-arrow-circle-up">Return to top</i>
                        </div>
                    </article>

                </div>


            </main>
            <br></br> <br></br><br></br>
            <footer>
                <div className="footer">
                    <h3>© Proximity Worldwide 2020. All Rights Reserved. Privacy Statement.</h3>

                    <div className="footer-icon">
                        <img className="fb-icon" src={Facebook} type="image/png"></img>
                        <img className="lknd-icon" src={LinkedIn} type="image/png"></img>


                    </div>
                </div>
            </footer>
        </div>

    )

}