import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './style.scss'
import Video from '../../assets/video.mp4'
import logo from '../../assets/logo.png'

export default function Navbar() {

    /*Function to set the navigation bar fixed when scrolled
    *using array destructuring 
    *declares new variable call "scrolled"
    *initial value false
    *setScrolled to update the value to true when the page is scrolled
    
    */
    const [scrolled, setScrolled] = useState(false);
    const handleScroll = () => {
        const y = window.scrollY;/*return the number of pixel that the page is scrolled veertically */
        if (y > 200) {
            setScrolled(true);
        } else {
            setScrolled(false)
        }
    }

    /* the fucntion handle passed to the use effect will run after the render is comitted to the page */
    useEffect(() => {
        window.addEventListener('scroll', handleScroll)
    })

    let x = ['navbar'];
    if (scrolled) {
        x.push('sticky');
    }


    const [isActive, setActive] = useState(false);
    const toggleClass = () => {
        setActive(!isActive);
    };


    //scroll to bottom function
    function scrollToBottom() {
        window.scroll({
            top: document.body.offsetHeight, /*returns height/width of the body in pixels including vertical padding and borders excluding margins*/
            left: 0,
            bottom: 0,
            behavior: 'smooth',

        });
    }

    return (
        <div className="container">
            <video autoPlay loop muted >
                <source src={Video} type="video/mp4" />
            </video>
            <div className={x.join("")}>
                <img className="logo" src={logo} type="image/png"></img>
                <nav>
                    <ul className={isActive ? "active" : "mobile-nav"}>
                        <div className="btn-lang">
                            <button>EN</button>
                            <button>FR</button>
                        </div><br></br><br></br>
                        <li>
                            <Link to="/">About</Link>
                        </li>
                        <li>
                            <Link to="/">Work</Link>
                        </li>
                        <li>
                            <Link to="/">Latest</Link>
                        </li>
                        <li>
                            <Link to="/">People & Careers</Link>
                        </li>
                        <li>
                            <Link to="/">Contact</Link>
                        </li>
                    </ul>
                    <div className="mobile-bar" onClick={toggleClass}>
                        <i className={isActive ? 'fas fa-times' : 'fas fa-bars'}></i>
                    </div>
                </nav>
            </div>
            <div className="quote"> <p>We make people more valuable to brands</p>
            </div>
            <div className="arrow-down bounce"><i className="fal fa-arrow-circle-down" onClick={scrollToBottom}></i>
            </div>

            <div className="cookie">
                <div className="cookie-text"><a>This website uses cookies to improve your experience. We'll assume you're ok with this, but you can opt-out if you wish.</a></div>
                
                <span className="cookie-content-right">
                    <button id="accept-cookie">Accept</button> <a href="#">Read More</a>
                </span>
                
        </div>
        </div>
    )
}
